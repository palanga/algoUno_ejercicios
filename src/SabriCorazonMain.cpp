#include <iostream>

#include "SabriCorazon.h"

#include "tipos.h"


int main(/*int argc, char const *argv[]*/) {
    std::vector<char> v;
    v.push_back('R');
    v.push_back('a');
    v.push_back('c');
    v.push_back('i');
    v.push_back('n');
    //v.push_back('g');

    std::cout << "palabra:   " << v << std::endl;
    invertir(v);
    std::cout << "invertida: " << v << std::endl;

    std::vector<char> milito;
    milito.push_back('M');
    milito.push_back('i');
    milito.push_back('l');
    milito.push_back('i');
    milito.push_back('t');
    milito.push_back('o');

    std::cout << "palabra:   " << milito << std::endl;
    invertir(milito);
    std::cout << "invertida: " << milito << std::endl;

    return 0;
}
