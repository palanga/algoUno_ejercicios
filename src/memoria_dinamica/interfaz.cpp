#include <iostream>

#include "interfaz.h"

using namespace std;

Lista* miLista = nullptr;

void menu_principal()
{
	bool quieroSalir = false;
	unsigned int opcion = -1;
	while ( !quieroSalir ) {
		limpiar_pantalla();
		cout << "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #" << endl;
		cout << "#                   M e n ú   p r i n c i p a l                   #" << endl;
		cout << "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #" << endl;
		cout << "#                                                                 #" << endl;
		cout << "#   [  1 ] Crear lista vacía                                      #" << endl;
		cout << "#                                                                 #" << endl;
		cout << "#   [  2 ] Primer elemento                                        #" << endl;
		cout << "#   [  3 ] Último elemento                                        #" << endl;
		cout << "#   [  4 ] Iesimo elemento                                        #" << endl;
		cout << "#   [  5 ] Largo de la lista                                      #" << endl;
		cout << "#                                                                 #" << endl;
		cout << "#   [  6 ] Insertar elementos                                     #" << endl;
		cout << "#   [  7 ] Quitar elemento                                        #" << endl;
		cout << "#                                                                 #" << endl;
		cout << "#   [ 97 ] Guardar                                                #" << endl;
		cout << "#   [ 98 ] Cargar                                                 #" << endl;
		cout << "#                                                                 #" << endl;
		cout << "#   [ 99 ] Imprimir                                               #" << endl;
		cout << "#                                                                 #" << endl;
		cout << "#     --                                                          #" << endl;
		cout << "#                                                                 #" << endl;
		cout << "#   [  0 ] Salir                                                  #" << endl;
		cout << "#                                                                 #" << endl;
		cout << "# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #" << endl;
		cout << endl;
		cout << "Elegí un número válido no te hagas el piola: "; cin  >> opcion;
		// selector:
		switch (opcion) {

			case 1: {// crear lista vacía:
				crear_lista_vacia();
				break;
			}

			case 2: {//primer elemento:
				primer_elemento();
				break;
			}

			case 3: {
				ultimo_elemento();
				break;
			}

			case 4: {
				iesimo_elemento();
				break;
			}

			case 5: {
				largo_de_la_lista();
				break;
			}

			case 6: {
				insertar_elementos();
				break;
			}

			case 7: {
				quitar_elementos();
				break;
			}

			case 0: {// salir:
				quieroSalir = true;
				break;
			}

			case 99: {
				imprimir();
				break;
			}

			default: {// fruta:
				break;
			}
		}
	}
}

// 1
void crear_lista_vacia()
{
	bool existeLista = miLista != nullptr;
	if ( existeLista ) {
		cout << "Ya tenés una lista!" << endl;
	}
	if ( !existeLista ) {
		Lista* nueva = new Lista;
		miLista = nueva;
	}
	pausa();
}

// 2
void primer_elemento()
{
	bool existeLista = miLista != nullptr;
	if ( !existeLista ) {
		cout << "No hay ninguna lista!!!" << endl;
	}
	if ( existeLista ) {
		bool tienePrimero = miLista->primero() != nullptr;
		if ( !tienePrimero ) {
			cout << "La lista está vacía!!!" << endl;
		}
		if ( tienePrimero ) {
			cout << miLista->primero()->e << endl;
		}
	}
	pausa();
}

// 3
void ultimo_elemento()
{
	bool existeLista = miLista != nullptr;
	if ( !existeLista ) {
		cout << "No hay ninguna lista!!!" << endl;
	}
	if ( existeLista ) {
		bool tieneUltimo = miLista->ultimo() != nullptr;
		if ( !tieneUltimo ) {
			cout << "La lista está vacía!!!" << endl;
		}
		if ( tieneUltimo ) {
			cout << miLista->ultimo()->e << endl;
		}
	}
	pausa();
}

// 4
void iesimo_elemento()
{
	bool existeLista = miLista != nullptr;
	if ( !existeLista ) {
		cout << "No hay ninguna lista!!!" << endl;
	}
	if ( existeLista ) {
		unsigned int n = miLista->largo();
		if (n == 0) {
			cout << "La lista está vacía!!!" << endl;
		}
		if (n > 0) {
			unsigned int i;
			cout << "qué elemento querés? "; cin >> i;
			bool iEnRango = i < n;// al ser unsigned siempre es positivo
			if (!iEnRango) {
				cout << "Te fuieste de rango con el indice!!! "
						"(el largo de la lista es: " << n << ")" << endl;
			}
			if (iEnRango) {
				cout << miLista->iesimo(i)->e << endl;
			}
		}
	}
	pausa();
}

// 5
void largo_de_la_lista()
{
	bool existeLista = miLista != nullptr;
	if ( !existeLista ) {
		cout << "No hay ningua lista!!!" << endl;
	}
	if ( existeLista ) {
		cout << miLista->largo() << endl;
	}
	pausa();
}

// 6
void insertar_elementos()
{
	bool existeLista = miLista != nullptr;
	if ( !existeLista ) {
		cout << "No hay ningua lista!!!" << endl;
	}
	if ( existeLista ) {

		char encolar = ' ';
		while ( encolar != 's'  &&  encolar != 'n' ) {
			cout << "Encolar? [s/n]: "; cin >> encolar;
		}

		if ( encolar == 's' ) {
			unsigned int n;
			cout << "Cuantos? "; cin >> n;
			int e;
			while (0 < n) {
				cout << "poné un entero: "; cin >> e;
				miLista->insertar(e);
				n--;
			}
		}

		if ( encolar == 'n' ) {
			unsigned int n;
			cout << "Cuantos?: "; cin >> n;
			unsigned int i;
			cout << "En qué posición?: "; cin >> i;
			unsigned int largo = miLista->largo();
			while ( largo < i ) {// no le pongo i < 0 porque i es unsigned
				cout << "Poné una opción válida (largo de la lista = " << largo << "): "; cin >> i;
			}
			int e;
			cout << "Poné los enteros en el orden que querés que aparezcan: " << endl;
			unsigned int c = 1;// c es para mostrar los elementos que van
			while (0 < n) {
				cout << c << ": "; cin >> e;
				miLista->insertar(e, i);
				i++;
				n--;
				c++;
			}
		}
	}
	pausa();
}

// 7
void quitar_elementos()
{// TODO quitar iesioms
	bool existeLista = miLista != nullptr;

	if ( !existeLista ) {
		cout << "¡Primero creá una lista!" << endl;
	}

	if ( existeLista ) {
		bool listaVacia = miLista->largo() == 0;

		if ( listaVacia ) {
			cout << "La lista ya está vacía." << endl;
		}

		if ( !listaVacia ) {
			int c = -1;
			unsigned int n = miLista->largo();
			cout << "Cuantos querés sacar?: "; cin >> c;
			bool cEnRango = 0 <= c  &&  c <= int(n);
			while ( !cEnRango ) {
				cout << "No puedo sacar " << c << " elementos, tengo " << n << ". Elegí otro número: "; cin >> c;
				cEnRango = 0 <= c  &&  c <= int(n);
			}

			if (c == 0) {
				cout << "Ok, no saco nada." << endl;
			}

			if (c == 1) {
				int i;
				cout << "¿Cuál?: "; cin >> i;
				bool iEnRango = 0 <= i && i < int(n);
				while ( !iEnRango ) {
					cout << "Te fuiste de rango. Poné un entero del 0 al " << n-1 << " inclusive: "; cin >> i;
					iEnRango = 0 <= i && i < int(n);
				}
				miLista->quitar(i);
			}

			if ( 1 < c ) {// TODO quitar iesiomos:
				cout << "Alta paja." << endl;
			}
		}
	}
	pausa();
}

// 99
void imprimir()
{
	bool existeLista = miLista != nullptr;
	if ( !existeLista ) {
		cout << "No hay ningua lista!!!" << endl;
	}
	if ( existeLista ) {
		cout << *miLista << endl;
	}
	pausa();
}

// pausa
void pausa()
{
	char opcion = 'n';
	while (opcion != 's') {
		cout << "¿Continuar? [s/n]: "; cin >> opcion;
	}
}

void limpiar_pantalla()
{
#ifdef WIN32
	system("cls");
#else
	system("clear");
#endif
}
