#ifndef TIPOS_H_INCLUDED
#define TIPOS_H_INCLUDED /*value*/

#include <iostream>
#include <vector>

/* El especialista tecnologico del escuadron de John Connor  */
/* ha estado trabajando en debuggear el codigo de los T1000. */
/* Hasta ahora, solo hemos podido des-ensamblar la siguiente */
/* estructura, que forma parte de dicho codigo.              */
struct cap_t {
	std::string essid;
	int power;
	std::string hash;
};

inline bool operator < (cap_t &a, cap_t &b) { return a.power < b.power; }
inline bool operator > (cap_t &a, cap_t &b) { return a.power > b.power; }
///////////////////////////////////////////////////////////////

template<class T>
void swap(std::vector<T> &v, int i, int j);

template<class T>
std::ostream & operator << (std::ostream &os, const std::vector<T> &v);

template<class T>
T minimoVector(const std::vector<T> &v);

template<class T>
T maximoVector(const std::vector<T> &v);


#endif /* TIPOS_H_INCLUDED */