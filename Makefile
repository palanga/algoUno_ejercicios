#------------------------------------------#
# Makefile para los ejercicios de algo uno #
#------------------------------------------#

#--------------------------------#
# descomentar el que voy a usar: #
#--------------------------------#

#USAR=SabriCorazon
USAR=tallerMati

CC=g++
CONFIG=-std=c++11
CFLAGS=-c
INCLUDES=
OBJETOS=$(USAR).o tipos.o busqueda.o sorting.o




.PHONY: all clean


all: clean $(USAR)All

clean:
	rm -f build/*.o
	rm -f $(USAR).dale

dale:
	./$(USAR).dale


#-------------------------------------------#
# objectos de Mati:                         #
#-------------------------------------------#

tallerMatiAll: tallerMati.o busqueda.o sorting.o tipos.o tallerMatiMain

tallerMatiMain:
	$(CC) $(CONFIG) src/tallerMatiMain.cpp build/tallerMati.o build/busqueda.o build/sorting.o build/tipos.o -o tallerMati.dale

tallerMati.o: src/tallerMati.h src/tallerMati.cpp
	$(CC) $(CONFIG) -c src/tallerMati.cpp -o build/tallerMati.o


#-------------------------------------------#
# objectos de Sabri:                        #
#-------------------------------------------#

SabriCorazonAll: SabriCorazon.o busqueda.o sorting.o tipos.o SabriCorazonMain

SabriCorazonMain:
	$(CC) $(CONFIG) src/SabriCorazonMain.cpp build/SabriCorazon.o build/busqueda.o build/sorting.o build/tipos.o -o SabriCorazon.dale

SabriCorazon.o: src/SabriCorazon.h src/SabriCorazon.cpp
	$(CC) $(CONFIG) -c src/SabriCorazon.cpp -o build/SabriCorazon.o


#-------------------------------------------#
# objectos generales de cualquier proyecto: #
#-------------------------------------------#

tipos.o: src/tipos.h src/tipos.cpp
	$(CC) $(CONFIG) -c src/tipos.cpp -o build/tipos.o

busqueda.o: src/busqueda.h src/busqueda.cpp
	$(CC) $(CONFIG) -c src/busqueda.cpp -o build/busqueda.o

sorting.o: src/sorting.h src/sorting.cpp
	$(CC) $(CONFIG) -c src/sorting.cpp -o build/sorting.o
