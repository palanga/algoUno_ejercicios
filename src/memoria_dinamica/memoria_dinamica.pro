TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    main.cpp \
    lista.cpp \
    interfaz.cpp

HEADERS += \
    lista.h \
    interfaz.h
