#ifndef BUSQUEDA_H_INCLUDED
#define BUSQUEDA_H_INCLUDED /*value*/

#include <vector>
#include <iostream>


template<class T>
int busquedaLineal(std::vector<T>& v, T e);

template<class T>
int busquedaBinaria(std::vector<T>& v, T e);

template<class T>
int busquedaBinaria(std::vector<T>& v, T e, int desde, int hasta);

/** /
template<class T>
int busquedaBinariaDesdeHasta(std::vector<T>& v, T e, int desde, int hasta);
/**/

template<class T>
int iDelMaximoRotada(std::vector<T>& v);

#endif /* BUSQUEDA_H_INCLUDED */