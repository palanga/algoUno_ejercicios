#ifndef LISTA_H
#define LISTA_H

#include <iostream>

class Lista
{
	private:
		struct Nodo {
				int e;
				Nodo* anterior;
				Nodo* posterior;
		};

	public:
		Lista();// 1
		Nodo* primero() const;// 2
		Nodo* ultimo() const;// 3
		Nodo* iesimo(unsigned int i) const;// 4
		unsigned int largo() const;// 5
		void insertar(int e);// 6.1
		void insertar(int e, unsigned int i);// 6.2
		void quitar();// 7.1
		void quitar(unsigned int i);// 7.2

		void guardar(std::ostream &os) const;// 97
		void cargar (std::ostream &is);// 98

		friend std::ostream &operator <<(std::ostream &os, const Lista &l);// 99

	private:
		Nodo* _primero;
		Nodo* _ultimo;
		unsigned int _largo;
};

#endif // LISTA_H
