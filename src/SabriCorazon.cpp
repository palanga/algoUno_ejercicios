#include "SabriCorazon.h"

#include "tipos.h"

template<class T>
void invertir(std::vector<T> &v) {
    int n = v.size();
    int i = 0;
    while (i < n/2) {
        swap(v, i, n-1-i);
        i++;
    }
}
// instancio:
template void invertir(std::vector<char> &v);
