#ifndef TALLER_MATI_H_INCLUDED
#define TALLER_MATI_H_INCLUDED

#include <iostream>
#include <vector>

using namespace std;

typedef vector<int> Naipes;


int posElemento(vector<int>& lista, int valor, int posDesde, int posHasta);

int posMaximo(vector<int>& lista);


bool contieneElementos(vector<int>& A,vector<int>& B);

Naipes naipesFaltantes(Naipes& cartas);

int dameBache(vector<int>& lista);

bool perteneceRotadas(vector<int>& lista, int elem);

int posElemento(vector<int>& lista,int elemento, int posDesde, int posHasta);

int posMaximo(vector<int>& lista);

#endif