#ifndef INTERFAZ_H
#define INTERFAZ_H

#include "lista.h"

void menu_principal();

void crear_lista_vacia();//   1
void primer_elemento();//     2
void ultimo_elemento();//     3
void iesimo_elemento();//     4
void largo_de_la_lista();//   5
void insertar_elementos();//  6
void quitar_elementos();//    7

void imprimir();

void pausa();
void limpiar_pantalla();

#endif // INTERFAZ_H
