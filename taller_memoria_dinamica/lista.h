#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED

#include <iostream>

using namespace std;

class Nodo{
  public:
    int elemento;
    Nodo* proximo;
};

class Lista{
  public:
		// Constructores
		Lista();
		Lista(int[], unsigned int);
		// Destructor
		~Lista();
		// Imprimir
		void mostrar(std::ostream &) const;
		// Comparar dos listas
		// Para comparar dos listas
		bool operator==(const Lista&);
		// Metodos que tienen que programar ustedes
		void agregarAdelante(int);
		void agregarAtras(int);
		unsigned int longitud(void) const;
		bool pertenece(int) const;
		int iesimo(int) const;
		int maximo(void) const;
		void borrar_iesimo(int);
		void reversa(void);
		void dividir_pares_impares(void);		
		Lista copiar(void) const;
		void mover(Nodo*, Nodo*);
				
	private:
    	Nodo* primero;
    	// auxiliares:
    	Nodo* iesimoNodo(unsigned int i);
};

// Para mostrar la lista
ostream &operator<<(ostream &, const Lista&);


#endif // LISTA_H_INCLUDED
