#ifndef SABRI_CORAZON_H_INCLUDED
#define SABRI_CORAZON_H_INCLUDED

#include <vector>

template<class T>
void invertir(std::vector<T> &v);

#endif
