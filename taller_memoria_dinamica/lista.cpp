
#include "lista.h"
#include <assert.h>  

using namespace std;

Lista::Lista(void){
	primero=NULL;
}

Lista::Lista(int v[], unsigned int size){

	// Si vector no tiene elementos se crea la lista vacia
	if (size == 0) primero = NULL;
	else{
		// Se agrega el primer elemento
		Nodo* pnodo = new Nodo;
		pnodo->elemento = v[0];
		pnodo->proximo = NULL;
		primero = pnodo;	
		// Se agregan el resto de los elementos
		for(unsigned int i = 1; i < size;i++){
			Nodo* pactual = pnodo;		
			pnodo = new Nodo;
			pnodo->elemento = v[i];   
			pnodo->proximo = NULL;
			pactual->proximo = pnodo;
		}
	}
}

Lista::~Lista(){
	Nodo* pnodo = this->primero;
	while(pnodo!=NULL){
		Nodo* pprox = pnodo->proximo;
		delete pnodo;
		pnodo=pprox;
	}
}

void Lista::mostrar(ostream &os) const{
	Nodo* pactual = primero;
	os << "[";
	while(pactual!=NULL) {
		os << pactual->elemento;
		pactual=pactual->proximo;
		if (pactual!=NULL) os << ",";
	}
	os << "]" << endl;
}

ostream & operator<<(ostream &os, const Lista& lista){
	lista.mostrar(os);	  
	return os;
}

bool Lista::operator==(const Lista& l){
	Nodo* pactual_this = this->primero;
	Nodo* pactual_l = l.primero;

	while(pactual_this != NULL && pactual_l != NULL){
		if (pactual_this->elemento != pactual_l->elemento) return false;
		pactual_this = pactual_this->proximo;
		pactual_l = pactual_l->proximo;
	}	
	return (pactual_this == pactual_l);
}

void Lista::agregarAdelante(int x){
	Nodo* nuevo = new Nodo {x, primero};
	primero = nuevo;
}

void Lista::agregarAtras(int x){
	Nodo* nuevo = new Nodo {x, NULL};
	// si l lista es vacía:
	if (primero == NULL) {
		primero = nuevo;
	} else {// sino la recorro hasta encontrar el último nodo:
		Nodo* actual = primero;
		while (actual->proximo != NULL) {
			actual = actual->proximo;
		}
		actual->proximo = nuevo;
	}
}

unsigned int Lista::longitud(void) const{
	Nodo* actual = primero;
	unsigned int res = 0;
	while (actual != NULL) {
		res++;
		actual = actual->proximo;
	}	
	return res;
}

bool Lista::pertenece(int x) const{
	Nodo* actual = primero;
	bool res = false;
	while (actual != NULL) {
		if (actual->elemento == x) {
			res = true;
		}
		actual = actual->proximo;
	}
	return res;
}

int Lista::iesimo(int i) const{
	Nodo* actual = primero;
	int j = 0;
	int res;
	while (actual != NULL) {
		if (j == i) {
			res = actual->elemento;
		}
		j++;
		actual = actual->proximo;
	}
	return res;
}

int Lista::maximo(void) const{
	Nodo* i = primero;
	Nodo* j = primero;
	int res = primero->elemento;
	while (i != NULL) {
		j = i;
		while (j != NULL) {
			if (j->elemento >= res) {
				res = j->elemento;
			}
			j = j->proximo;
		}
		i = i->proximo;
	}
	return res;
}

void Lista::borrar_iesimo(int i){
	Nodo* aBorrar = iesimoNodo(i);
	int n = longitud();
	bool loBorre = false;
	// lista de un sólo elemento:
	if (n == 1  &&  !loBorre) {
		primero = NULL;
		delete aBorrar;
		aBorrar = NULL;
		loBorre = true;
	}
	// lista de más de un elemento y quiero borrar el último:
	if (n > 1  &&  i == n-1  &&  !loBorre) {
		Nodo* elAnterior = iesimoNodo(i-1);
		elAnterior->proximo = NULL;
		delete aBorrar;
		aBorrar = NULL;
		loBorre = true;
	}
	// lista de más de un elemento y quiero borrar el primero:
	if (n > 1  &&  i == 0  &&  !loBorre) {
		Nodo* elSiguiente = iesimoNodo(i+1);
		primero = elSiguiente;
		delete aBorrar;
		aBorrar = NULL;
		loBorre = true;
	}
	// lista de más de dos elementos e i está en el medio:
	if (n > 2  &&  i > 0  &&  i < n-1  &&  !loBorre) {
		Nodo* elAnterior = iesimoNodo(i-1);
		Nodo* elSiguiente = iesimoNodo(i+1);
		elAnterior->proximo = elSiguiente;
		delete aBorrar;
		aBorrar = NULL;
		loBorre = true;
	}
}

Nodo* Lista::iesimoNodo(unsigned int i) {
	Nodo* actual = primero;
	unsigned int j = 0;
	Nodo* res = actual;
	bool loEncontre = false;
	while ( actual != NULL  &&  !loEncontre) {
		if (j == i) {
			res = actual;
			loEncontre = true;
		}
		j++;
		actual = actual->proximo;
	}
	return res;
}

void Lista::reversa(void){
	unsigned int n = longitud() - 1;
	Nodo* ultimo = iesimoNodo(n);
	while (n > 0) {
		iesimoNodo(n)->proximo = iesimoNodo(n-1);
		n--;
	}
	iesimoNodo(n)->proximo = NULL;
	primero = ultimo;
}

void Lista::dividir_pares_impares(void){
	/* v2: no sirve porque esta mal mover * /
	unsigned int n = longitud();
	unsigned int i = 0;
	Nodo* actual = primero;
	Nodo* ultimo = iesimoNodo(n-1);
	while ( i < n ) {
		if ( actual->elemento % 2 != 0 ) {
			mover(actual, ultimo);
			ultimo = actual;
		}
		actual = actual->proximo;
		i++;
	}/**/
	/* v1 */
	unsigned int n = longitud();
	unsigned int i = 0;
	// agrego atras los pares:
	while (i < n) {
		if (iesimo(i) % 2 == 0) {
			agregarAtras(iesimo(i));
		}
		i++;
	}
	// agrego atras los impares:
	i = 0;
	while (i < n) {
		if (iesimo(i) % 2 != 0) {
			agregarAtras(iesimo(i));
		}
		i++;
	}
	// borro los primeros ene elementos de la lista:
	i = 0;
	while (i < n) {
		borrar_iesimo(0);
		i++;
	}/**/
}

Lista Lista::copiar(void) const{
	Lista* nueva = new Lista;
	Nodo* actual = primero;
	while (actual != NULL) {
		nueva->agregarAtras(actual->elemento);
		actual = actual->proximo;
	}
	return *nueva;
}

/* esto esta mal: * /
void Lista::mover(Nodo* aMover, Nodo* despuesDe) {
	unsigned int n = longitud();
	bool loMovi = false;
	// lista de un sólo elemento:
	if (n == 1  &&  !loMovi) {
		loMovi = true;
	}
	// lista de dos elementos y quiero mover el primero:
	if (n == 2  &&  aMover == iesimoNodo(0)  &&  !loMovi) {
		aMover->proximo = NULL;
		despuesDe->proximo = aMover;
		primero = despuesDe;
		loMovi = true;
	}
	// lista de dos elementos y quiero mover el último:
	if (n == 2  &&  aMover == iesimoNodo(n-1)  &&  !loMovi) {
		loMovi = true;
	}
	// lista de más de dos elementos y quiero mover el primero:
	if (n > 2  &&  aMover == iesimoNodo(0)  &&  !loMovi) {
		primero = aMover->proximo;
		aMover->proximo = despuesDe->proximo;
		despuesDe->proximo = aMover;		
		loMovi = true;
	}
	// lista de más de dos elementos y quiero mover el no primero:
	if (n > 2  &&  aMover != iesimoNodo(0)  &&  !loMovi) {
		unsigned int i = 0;
		while (i < n) {
			if (aMover == iesimoNodo(i)) {
				break;
			}
			i++;
		}
		Nodo* elAnterior = iesimoNodo (i-1);
		elAnterior->proximo = aMover->proximo;
		aMover->proximo = despuesDe->proximo;
		despuesDe->proximo = aMover;		
		loMovi = true;
	}
}/**/