#include "sorting.h"
#include "tipos.h"

#include <vector>
#include <iostream>


template<class T>
T minimoVector(const std::vector<T> &v)
{
	int n = v.size();
	int i = 1;

	T res = v[0];

	while (i < n)
	{
		if (v[i] < res)
		{
			res =v[i];
		}
		i++;
	}

	return res;
}
// instanciar:
template int minimoVector<int>(const std::vector<int> &v);
template float minimoVector<float>(const std::vector<float> &v);
template char minimoVector<char>(const std::vector<char> &v);

template<class T>
T maximoVector(const std::vector<T> &v)
{
	int n = v.size();
	int i = 1;
	T res = v[0];

	while (i < n)
	{
		if (v[i] > res)
		{
			res =v[i];
		}
		i++;
	}

	return res;
}
// instanciar:
template int maximoVector<int>(const std::vector<int> &v);
template float maximoVector<float>(const std::vector<float> &v);
template char maximoVector<char>(const std::vector<char> &v);


/*
 *  algoritmos de ordenamiento  - - - - - - - - - -
*/
template<class T>
void bubbleSort(std::vector<T> &v)
{
	int i = 0, j = 0;
	int n = v.size();

	bool ordenado = false;

	while (i < n && !ordenado)
	{
		j = 0;
		ordenado = true;
		while (j < n - 1)
		{
			if ( v[j] > v[j+1] )
			{
				swap(v, j, j+1);
				ordenado = false;
			}
			j++;
		}
		i++;
	}
}
// instanciar:
template void bubbleSort(std::vector<int> &v);
template void bubbleSort(std::vector<float> &v);
template void bubbleSort(std::vector<char> &v);
template void bubbleSort(std::vector<cap_t> &v);


template<class T>
void insertionSort(std::vector<T> &v)
{
	int n = v.size();
	int i = 0, j = 0;

	T key;

	while (i < n - 1)
	{
		j = i + 1;
		key = v[j];

		while (j > 0 && v[j-1] > key)
		{
			v[j] = v[j-1];
			j--;
		}
		v[j] = key;
		i++;
	}
}
// instanciar:
template void insertionSort<int>(std::vector<int> &v);
template void insertionSort<float>(std::vector<float> &v);
template void insertionSort<char>(std::vector<char> &v);


void countingSort(std::vector<int> &v)
{
	int minimo = minimoVector(v);
	int n = v.size();
	int i = 0, j = 0, k = 0;
	// normalizo el vector:
	while (k < n)
	{
		v[k] -= minimo;
		k++;
	}
	// saco el máximo del vector normalizado y creo al contador con ese tamaño:
	int maximo = maximoVector(v);
	std::vector<int> counter(maximo + 1);
	// cuento:
	while (i < n)
	{
		counter[v[i]]++;
		i++;
	}
	// sobreescribo v con sus valores ordenados y normalizados:
	j = counter.size() - 1;
	i = n - 1;
	while (j >= 0)
	{
		while (counter[j] > 0)
		{
			v[i] = j;
			counter[j]--;
			i--;
		}
		j--;
	}
	// desnormalizo v:
	k = 0;
	while (k < n)
	{
		v[k] += minimo;
		k++;
	}
}
