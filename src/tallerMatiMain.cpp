//#include "tallerMati.cpp"
#include <ctime>
#include <sstream>
#include <signal.h>

#include <algorithm>

#include "tallerMati.h"

#include "tipos.h"
#include "sorting.h"

/*
*	 El siguiente archivo contiene los tests correspondientes a los cuatro 
*	ejercicios presentes en ejercicios.cpp
*/

//La siguiente variable aumenta el tamaño de las instancias de prueba grandes. (Default = 0)
const int AUMENTAR_TAM = 0;
//La siguiente variable dice si se corren los test grandes. (Default = false)
const bool CORRER_GRANDES = true;

void segfault_sigaction(int signal)
{
	cout<<endl;
    cout<<"     Hubo un error de indices"<<endl;
    cout<<endl;
    cout<<endl;
    exit(0);
}

string numberToString ( int Number )
  {
     ostringstream ss;
     ss << Number;
     return ss.str();
  }

string imprimir(vector<int> vector){
	string salida = "[";
	for (int i=0;i<vector.size();++i)
		salida += " "+ numberToString(vector[i]) +" ";
	salida += "]";
	return salida;
}

string test_recom(){
	bool error = false;
	string salida = "Falló con los casos : \n";

	
	//Lista vacia
	vector<int> A;
	int res = posElemento(A,1,0,0);
	if (res!=-1){
		error = true;
		salida += "    posElemento("+imprimir(A)+",1,0,0) se esperaba -1 y se obtuvo "+numberToString(res)+"\n";
	}
	A.push_back(1);
	res = posElemento(A,1,0,0);
	if (res!=0){
		error = true;
		salida += "    posElemento("+imprimir(A)+",1,0,0) se esperaba 0 y se obtuvo "+numberToString(res)+"\n";
	}
	A.push_back(4);
	res = posElemento(A,4,0,1);
	if (res!=1){
		error = true;
		salida += "    posElemento("+imprimir(A)+",4,0,1) se esperaba 1 y se obtuvo "+numberToString(res)+"\n";
	}
	res = posElemento(A,1,0,1);
	if (res!=0){
		error = true;
		salida += "    posElemento("+imprimir(A)+",1,0,0) se esperaba 0 y se obtuvo "+numberToString(res)+"\n";
	}


	A.clear();
	A.push_back(1);
	A.push_back(2);
	A.push_back(7);
	A.push_back(15);
	res = posElemento(A,7,0,3);
	if (res != 2){
		error = true;
		salida += "    posElemento("+imprimir(A)+",7,0,3) se esperaba 2 y se obtuvo "+numberToString(res)+"\n";
	}
	res =posElemento(A,7,1,3);
	if (res!=2){
		error = true;
		salida += "    posElemento("+imprimir(A)+",7,1,3) se esperaba 2 y se obtuvo "+numberToString(res)+"\n";
	}
	res =posElemento(A,9,1,3);
	if (res!=-1){
		error = true;
		salida += "    posElemento("+imprimir(A)+",9,1,3) se esperaba -1 y se obtuvo "+numberToString(res)+"\n";
	}
	res =posElemento(A,15,0,2);
	if (res!=-1){
		error = true;
		salida += "    posElemento("+imprimir(A)+",15,0,2) se esperaba -1 y se obtuvo "+numberToString(res)+"\n";
	}

	A.clear();
	A.push_back(1);
	res =posMaximo(A);
	if (res!=0){
		error = true;
		salida += "    posMaximo("+imprimir(A)+") se esperaba 0 y se obtuvo "+numberToString(res)+"\n";
	}
	A.push_back(5);
	A.push_back(10);
	res = posMaximo(A);
	if (res!=2){
		error = true;
		salida += "    posMaximo("+imprimir(A)+") se esperaba 2 y se obtuvo "+numberToString(res)+"\n";
	}


	if (!error)
		return "OK";
	return salida;
}
  
string test_ej1(){
	bool error = false;
	string salida = "Falló con los casos : \n";

	
	//Lista vacia
	vector<int> A;
	vector<int> B;
	if (!contieneElementos(A,B)){
		error = true;
		salida += "    ("+imprimir(A)+","+imprimir(B)+") se esperaba True \n";
	}

	//Elementos repetidos
	A.clear();
	B.clear();
	A.push_back(0);
	A.push_back(1);
	A.push_back(1);
	B.push_back(0);
	B.push_back(1);
	B.push_back(2);
	if (!contieneElementos(A,B)){
		error = true;
		salida += "    ("+imprimir(A)+","+imprimir(B)+") se esperaba True \n";
	}

	//Caso negativo
	A.clear();
	B.clear();
	A.push_back(3);
	A.push_back(6);
	A.push_back(8);
	B.push_back(3);
	B.push_back(6);
	B.push_back(7);
	if (contieneElementos(A,B)){
		error = true;
		salida += "    ("+imprimir(A)+","+imprimir(B)+") se esperaba False \n";
	}

	//Caso A vacio
	A.clear();
	B.clear();
	B.push_back(3);
	B.push_back(6);
	B.push_back(7);
	if (!contieneElementos(A,B)){
		error = true;
		salida += "    ("+imprimir(A)+","+imprimir(B)+") se esperaba True \n";
	}
	
	//Casos Grandes
	if (CORRER_GRANDES){
		A.clear();
		B.clear();
		for (int i=0;i<100000+AUMENTAR_TAM;i+=2)
			B.push_back(i);
		A.push_back(500);
		A.push_back(5510);
		A.push_back(99990);
		if (!contieneElementos(A,B)){
			error = true;
			salida += "    ( ... , ... ) se esperaba True \n";
		}

		A.clear();
		A.push_back(500);
		A.push_back(5510);
		A.push_back(99991);
		if (contieneElementos(A,B)){
			error = true;
			salida += "    ( ... , ... ) se esperaba False \n";
		}
	}

	if (!error)
		return "OK";
	return salida;
}

string test_ej2(){
	bool error = false;
	string salida = "Falló con los casos : \n";

	//Lista vacia
	vector<int> A;
	vector<int> B;
	B.push_back(1);
	B.push_back(2);
	B.push_back(3);
	B.push_back(4);
	B.push_back(5);
	B.push_back(6);
	B.push_back(7);
	B.push_back(8);
	B.push_back(9);
	B.push_back(10);
	B.push_back(11);
	B.push_back(12);
	vector<int> res = naipesFaltantes(A); 
	if (B!=res){
		error = true;
		salida += "    "+imprimir(A)+" retornó: "+imprimir(res)+" y esperaba: "+imprimir(B)+"\n";
	}

	A.clear();
	B.clear();
	//caso comun
	A.push_back(1);
	A.push_back(2);
	A.push_back(3);
	B.push_back(4);
	B.push_back(5);
	B.push_back(6);
	A.push_back(7);
	B.push_back(8);
	A.push_back(9);
	B.push_back(10);
	B.push_back(11);
	A.push_back(12);
	res = naipesFaltantes(A); 
	if (B!=res){
		error = true;
		salida += "    "+imprimir(A)+" retornó: "+imprimir(res)+" y esperaba: "+imprimir(B)+"\n";
	}

	res = naipesFaltantes(B); 
	if (A!=res){
		error = true;
		salida += "    "+imprimir(B)+" retornó: "+imprimir(res)+" y esperaba: "+imprimir(A)+"\n";
	}
	

	if (!error)
		return "OK";
	return salida;
}

string test_ej3(){
	bool error = false;
	string salida = "Falló con los casos : \n";

	vector<int> A;
	A.push_back(0);
	A.push_back(2);
	A.push_back(3);
	int res = dameBache(A);
	if (res!=1){
		error = true;
		salida += "    "+imprimir(A)+" retornó: "+ numberToString(res)+" y se esperaba: 1 \n";
	}

	A.clear();
	A.push_back(3);
	A.push_back(-1);
	A.push_back(2);
	A.push_back(-2);
	A.push_back(0);
	res = dameBache(A);
	if (res!=1){
		error = true;
		salida += "    "+imprimir(A)+" retornó: "+ numberToString(res)+" y se esperaba: 1 \n";
	}

 	A.clear();
	A.push_back(-2);
	A.push_back(-1);
	A.push_back(1);
	A.push_back(2);
	res = dameBache(A);
	if (res!=0){
		error = true;
		salida += "    "+imprimir(A)+" retornó: "+ numberToString(res)+" y se esperaba: 0 \n";
	}

	A.clear();
	A.push_back(-500);
	A.push_back(-499);
	A.push_back(-497);
	A.push_back(-496);
	res = dameBache(A);
	if (res!=-498){
		error = true;
		salida += "    "+imprimir(A)+" retornó: "+ numberToString(res)+" y se esperaba: -498 \n";
	}

	A.clear();
	A.push_back(100);
	A.push_back(101);
	A.push_back(102);
	A.push_back(104);
	res = dameBache(A);
	if (res!=103){
		error = true;
		salida += "    "+imprimir(A)+" retornó: "+ numberToString(res)+" y se esperaba: 103 \n";
	}

	A.clear();
	A.push_back(101);
	A.push_back(102);
	A.push_back(103);
	res = dameBache(A);
	if (res!=104){
		error = true;
		salida += "    "+imprimir(A)+" retornó: "+ numberToString(res)+" y se esperaba: 104 \n";
	}

	A.clear();
	A.push_back(23);
	res = dameBache(A);
	if (res!=24){
		error = true;
		salida += "    "+imprimir(A)+" retornó: "+ numberToString(res)+" y se esperaba: 24 \n";
	}

	/**/
	if (CORRER_GRANDES){
		A.clear();
		for (int i=10;i<10000+AUMENTAR_TAM;i++){
			if (i!=9957)
				A.push_back(i);
		}
		random_shuffle(A.begin(), A.end());
		res = dameBache(A);
		if (res!=9957){
			error = true;
			salida += "    [Lista grande] retornó: "+ numberToString(res)+" y se esperaba: 9957 \n";
		}
	}/**/

	if (!error)
		salida = "OK";

	return salida;
}

string test_ej3B(){
	bool error = false;
	string salida = "Falló con los casos : \n";

	vector<int> A;

	struct sigaction act;
    act.sa_handler = segfault_sigaction;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    sigaction(SIGSEGV, &act, 0);

	A.clear();
	A.push_back(1);
	A.push_back(2);
	A.push_back(102);
	A.push_back(104);

	int res = dameBache(A);
	if (res!=3){
		error = true;
		salida += "\n    "+imprimir(A)+" retornó: "+ numberToString(res)+" y se esperaba: 3 ";
	}

	A.clear();
	A.push_back(-1);
	A.push_back(-3);
	A.push_back(-2);
	A.push_back(5);

	res = dameBache(A);
	if (res!=0){
		error = true;
		salida += "\n    "+imprimir(A)+" retornó: "+ numberToString(res)+" y se esperaba: 0 ";
	}

	if (!error)
		return "OK";
	
	return salida;
}

/**/
string test_ej4(){
	bool error = false;
	string salida = "Falló con los casos : \n";

	vector<int> A;

	if (perteneceRotadas(A,4)){
		error = true;
		salida += "    "+imprimir(A)+" para 4 se esperaba False \n";
	}

	A.push_back(3);
	A.push_back(1);

	if (!perteneceRotadas(A,3)){
		error = true;
		salida += "    "+imprimir(A)+" para 3 se esperaba True \n";
	}

	A.push_back(2);
	if (!perteneceRotadas(A,1)){
		error = true;
		salida += "    "+imprimir(A)+" para 1 se esperaba True \n";
	}

	if (perteneceRotadas(A,4)){
		error = true;
		salida += "    "+imprimir(A)+" para 4 se esperaba False \n";
	}

	A.clear();
	A.push_back(500);
	A.push_back(-10);
	A.push_back(2);
	A.push_back(497);
	A.push_back(498);
	A.push_back(499);
	if (!perteneceRotadas(A,497)){
		error = true;
		salida += "    "+imprimir(A)+" para 497 se esperaba True \n";
	}

	if (!perteneceRotadas(A,-10)){
		error = true;
		salida += "    "+imprimir(A)+" para -10 se esperaba True \n";
	}

	A.clear();
	A.push_back(500);
	A.push_back(600);
	A.push_back(700);
	A.push_back(800);
	A.push_back(801);
	A.push_back(1);
	if (!perteneceRotadas(A,1)){
		error = true;
		salida += "    "+imprimir(A)+" para 1 se esperaba True \n";
	}

	if (!perteneceRotadas(A,700)){
		error = true;
		salida += "    "+imprimir(A)+" para 700 se esperaba True \n";
	}

	if (perteneceRotadas(A,-10)){
		error = true;
		salida += "    "+imprimir(A)+" para -10 se esperaba False \n";
	}

	if (CORRER_GRANDES){
		A.clear();
		for (int i=50000+AUMENTAR_TAM;i<100000+AUMENTAR_TAM;i+=2)
			A.push_back(i);
		for (int i=0;i<50000+AUMENTAR_TAM;i+=2)
			A.push_back(i);

		if (perteneceRotadas(A,90001)){
			error = true;
			salida += "    [Lista de pares] para 90001 se esperaba False \n";
		}

		if (!perteneceRotadas(A,90000)){
			error = true;
			salida += "    [Lista de pares] para 90000 se esperaba True \n";
		}

		if (!perteneceRotadas(A,99998)){
			error = true;
			salida += "    [Lista de pares] para 99998 se esperaba True \n";
		}

		if (!perteneceRotadas(A,99996)){
			error = true;
			salida += "    [Lista de pares] para 99996 se esperaba True \n";
		}

		if (!perteneceRotadas(A,49998)){
			error = true;
			salida += "    [Lista de pares] para 49998 se esperaba True \n";
		}

		if (perteneceRotadas(A,49997)){
			error = true;
			salida += "    [Lista de pares] para 49997 se esperaba False \n";
		}
	}

	if (!error)
		return "OK";
	return salida;
}/**/


int main(){

  	clock_t begin;
	clock_t end;
	double segundos;

	cout.precision(3);
	cout << fixed;
	cout<<endl;
	cout<<"Funciones Recomendadas: .. ";
	cout<<test_recom()<<endl;
	cout<<endl;
	cout<<endl;
	cout<<"Ejercicio 1 .. ";
	begin = clock();
	cout<<test_ej1()<<endl;
	end = clock();
	segundos = double(end - begin) / CLOCKS_PER_SEC;
	cout<<"  Segundos: "<<segundos<<endl;
	cout<<endl;
	cout<<endl;
	cout<<"Ejercicio 2 .. ";
	begin = clock();
	cout<<test_ej2()<<endl;
	end = clock();
	segundos = double(end - begin) / CLOCKS_PER_SEC;
	cout<<"  Segundos: "<<segundos<<endl;
	cout<<endl;
	cout<<endl;
	cout<<"Ejercicio 3 .. ";
	begin = clock();
	cout<<test_ej3()<<endl;
	end = clock();
	segundos = double(end - begin) / CLOCKS_PER_SEC;
	cout<<"  Segundos: "<<segundos<<endl;
	cout<<endl;
	cout<<endl;
	cout<<"Ejercicio 4 .. ";
	begin = clock();
	cout<<test_ej4()<<endl;
	end = clock();
	segundos = double(end - begin) / CLOCKS_PER_SEC;
	cout<<"  Segundos: "<<segundos<<endl;
	cout<<endl;
	cout<<endl;
	cout<<"Ejercicio 3 Bis .. ";
	cout<<test_ej3B()<<endl;
	cout<<endl;
	cout<<endl;

	return 0;
}
