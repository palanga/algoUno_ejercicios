#include <iostream>
#include <cassert>
#include "lista.h"
#include "test/cases.h"

using namespace std;

int main()
{
    int exitos = 0;
    int total = 10;
    exitos += test01_agregarAdelante() ? 1 : 0;
    exitos += test02_agregarAtras() ? 1 : 0;
    exitos += test03_longitud() ? 1 : 0;
    exitos += test04_pertenece() ? 1 : 0;
    exitos += test05_iesimo() ? 1 : 0;
    exitos += test06_maximo() ? 1 : 0;
    exitos += test07_borrar_iesimo() ? 1 : 0;
    exitos += test08_reversa() ? 1 : 0;
    exitos += test09_divirdir_pares_impares() ? 1 : 0;
    exitos += test10_copiar() ? 1 : 0;
    
    cout << "# Ejercicios correctos: " << exitos << endl;
    cout << "# Ejercicios incorrectos: " << total - exitos << endl;

    
}

