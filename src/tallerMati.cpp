#include "tallerMati.h"

#include "sorting.h"
#include "tipos.h"
#include "busqueda.h"

using namespace std;

/*
* Para correr:  g++ tests.cpp -o tests
*				./tests
*/


//******************************

//FUNCIONES RECOMENDADAS para hacer:

//  Retorna la posicion de un elemento en la lista entre la posicion "posDesde"
// y posHasta, o -1 si no se encuentra
int posElemento(vector<int>& lista, int valor, int posDesde, int posHasta)
{
	int res = -1;
	int i = posDesde;
	if (lista.size() != 0) {
		while (i <= posHasta)
		{
			if (lista[i] == valor)
			{
				res = i;
				break;
			}
			i++;
		}
	}
	return res;
}

//Retorna la posicion del elemento de maximo valor en la lista. (Require lista con elementos)
int posMaximo(vector<int>& lista)
{
	int res = 0;
	int i = 0;
	while (i < lista.size())
	{
		if (lista[i] > lista[res])
		{
			res = i;
		}
		i++;
	}
	return res;
}

//******************************


// Ejercicio 1
// A es subconjunto de B ?
bool contieneElementos(vector<int>& A,vector<int>& B) {
	bool res;
	int j;
	int i = 0;
	while (i < A.size()) {
		j = 0;
		res = false;
		while (j < B.size() && !res) {
			if (A[i] == B[j]) {
				res = true;
			}
			j++;
		}
		i++;
	}
	if (A.size() == 0) {
		res = true;
	}
	return res;
}

// Ejercicio 2
// Nota: Naipes esta definido como vector<int>
Naipes naipesFaltantes(Naipes& cartas){
	Naipes res;
	int i = 1;
	int j = 0;
	bool loEncontro = false;
	while (i <= 12) {
		j = 0;
		loEncontro = false;
		while (j < cartas.size() && !loEncontro) {
			if (i == cartas[j]) {
				loEncontro = true;
			}
			j++;
		}
		if (!loEncontro) {
			res.push_back(i);
		}
		i++;
	}
	return res;
}

// Ejercicio 3
int dameBache(vector<int>& lista) {
	countingSort(lista);
	int res = maximoVector(lista) + 1;
	int i = minimoVector(lista);
	bool yata = false;
	while ( i < maximoVector(lista)  &&  !yata ) {
		if (lista[i - minimoVector(lista)] != i ) {
			yata = true;
			res = i;
		}
		i++;
	}
	if (lista.size() == 0) {
		res = 0;
	}

	return res;
}


// Ejercicio 4
bool perteneceRotadas(vector<int>& lista, int elem) {
	// longitud de la lista:
	int n = lista.size();
	// me fijo si es la lista vacia:
	bool esVacia = n == 0;
	// busco (binaria) el indice del máximo. si la lista es vacía devuelve -1:
	int iDelMax = -1;
	if ( !esVacia ) {
		iDelMax = iDelMaximoRotada(lista);
	}
	// busco en que mitad de la lista debería estar elem:
	// porimero, si no es lista vacia, me fijo si esta al principio:
	bool estaAlPrincipio = false;
	if ( !esVacia ) {
		estaAlPrincipio = ( lista[0] <= elem )  &&  ( elem <= lista[iDelMax] );
	}
	// si no me voy de rango busco si esta al final:
	bool estaAlFinal = false;
	if ( iDelMax + 1 < n ) {
		estaAlFinal = ( lista[iDelMax+1] <= elem )  &&  ( elem <= lista[n-1] );
	}
	// si esta al principio hago busqueda binaria ahí,
	// sino hago busqueda binaria al final,
	// y sino no hago nada y res (loEncontre) es false:
	bool loEncontre = false;
	if ( estaAlPrincipio ) {
		loEncontre = busquedaBinaria(lista, elem, 0, iDelMax) != -1;
	}
	if ( estaAlFinal ) {
		loEncontre = busquedaBinaria(lista, elem, iDelMax+1, n-1) != -1;
	}
	return loEncontre;
}