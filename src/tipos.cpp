#include "tipos.h"

/*
 *  swap para vector genérico de T
*/
template<class T>
void swap(std::vector<T> &v, int i, int j)
{
	T aux = v[i];
	v[i] = v[j];
	v[j] = aux;
}
// instanciar:
template void swap<int>(std::vector<int> &v, int i, int j);
template void swap<float>(std::vector<float> &v, int i, int j);
template void swap<char>(std::vector<char> &v, int i, int j);
template void swap<cap_t>(std::vector<cap_t> &v, int i, int j);

/*
 *  operador << para vector de T
*/
template<class T>
std::ostream & operator << (std::ostream &os, const std::vector<T> &v)
{
	os << '[' << ' ';

	int n = v.size();
	int i = 0;
	while (i < n)
	{
		os << v[i] << ' ';
		i++;
	}

	os << ']';
}
// instanciar:
template std::ostream & operator << (std::ostream &os, const std::vector<int> &v);
template std::ostream & operator << (std::ostream &os, const std::vector<float> &v);
template std::ostream & operator << (std::ostream &os, const std::vector<char> &v);
