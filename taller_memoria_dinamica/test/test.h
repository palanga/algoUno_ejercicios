#ifndef TEST_INCLUDED_H
#define TEST_INCLUDED_H

#include <iostream>
#include <sstream>
#include <string>
#include <cassert>
#include <cmath> 

using namespace std;

void printBeginTest(int testNumber, string testName){
    cout << "Corriendo test " << testNumber << " " << testName << endl;
}

void printEndTest(int testNumber, string testName){
    cout << "Finalizado test " << testNumber << " " << testName << endl;
}

template<typename T>
void printFail(const T calc, const T orig, bool debug){
    if(debug){
        cout << "Falló caso: " << std::boolalpha << calc << " != " << std::boolalpha << orig << endl;    
    }else{
        cout << "Falló caso extra" << endl; 
    }
}


template<typename T>
bool asegurar(const T calc, const T orig, bool debug = true){
    if(calc != orig){
        printFail(calc,orig, debug);
        return false;
    }else{
        return true;
    }
}


#endif //TEST_INCLUDED_H
