#include <iostream>
#include "cases.h"
#include "test.h"
#include "../lista.h"

using namespace std;

bool test01_agregarAdelante(){
    unsigned int testNumber = 1;
    string testName = "agregarAdelante";    
    printBeginTest(testNumber, testName);

    int a[5] = {123, 3, 0, -1, -123};
		int b[4] = {3, 0, -1, -123};
		Lista l1(a, 5);
		Lista l2(b, 4);
		l2.agregarAdelante(123);

		int c[1] = {-123};
		Lista l3(c, 1);		
		Lista l4;
		l4.agregarAdelante(-123);
		
    bool res = asegurar(l1 == l2, true);
		res &= asegurar(l3 == l4, true); 
    return res;
}

bool test02_agregarAtras(){
		unsigned int testNumber = 2;
    string testName = "agregarAtras";    
    printBeginTest(testNumber, testName);

    int a[5] = {123, 3, 0, -1, -123};
		int b[4] = {123, 3, 0, -1};
		Lista l1(a, 5);
		Lista l2(b, 4);
		l2.agregarAtras(-123);

		int c[1] = {-123};
		Lista l3(c, 1);		
		Lista l4;
		l4.agregarAtras(-123);
		

    bool res = asegurar(l1 == l2, true);
		res &= asegurar(l3 == l4, true);    
		return res;
}

bool test03_longitud(){
		unsigned int testNumber = 3;
    string testName = "longitud";    
    printBeginTest(testNumber, testName);

		int a[5] = {123, 3, 0, -1, -123};
		Lista l1(a,5);
		Lista l2;

 		bool res = asegurar(l1.longitud(), unsigned(5));
 		res &= asegurar(l2.longitud(), unsigned(0));				
		return res;
}

bool test04_pertenece(){
		unsigned int testNumber = 4;
    string testName = "pertenece";    
    printBeginTest(testNumber, testName);

		int a[5] = {123, 3, 0, -1, -123};
		Lista l1(a,5);
		Lista l2;
		
 		bool res = asegurar(l1.pertenece(0), true);
 		res &= asegurar(l2.pertenece(123), false);				
		return res;
}

bool test05_iesimo(){
		unsigned int testNumber = 5;
    string testName = "iesimo";    
    printBeginTest(testNumber, testName);

		int a[5] = {123, 3, 0, -1, -123};
		Lista l1(a,5);
		Lista l2;
		
 		bool res = asegurar(l1.iesimo(0), 123);
 		res &= asegurar(l1.iesimo(4), -123);				
		return res;
}

bool test06_maximo(){
		unsigned int testNumber = 6;
    string testName = "maximo";    
    printBeginTest(testNumber, testName);

		int a[5] = {123, 3, 0, -1, -123};
		int b[5] = {-123};
		Lista l1(a,5);
		Lista l2(b,1);
		
 		bool res = asegurar(l1.maximo(), 123);
 		res &= asegurar(l2.maximo(), -123);				
		return res;
}

bool test07_borrar_iesimo(){
		unsigned int testNumber = 7;
    string testName = "borrar_iesimo";    
    printBeginTest(testNumber, testName);

		int a[5] = {123, 3, 0, -1, -123};
		int b[4] = {123, 3, 0, -1};
		Lista l1(a,5);
		Lista l2(b,4);
		l1.borrar_iesimo(4);


		int c[5] = {123, 3, 0, -1, -123};
		int d[4] = {123, 3, 0, -123};
		Lista l3(c,5);
		Lista l4(d,4);
		l3.borrar_iesimo(3);
		
		int e[1] = {123};
		Lista l5(e,1);
		Lista l6;
		l5.borrar_iesimo(0);
		
		
 		bool res = asegurar(l1 ==  l2, true);
 		res &= asegurar(l3 == l4, true);
		res &= asegurar(l5 == l6, true);				
		return res;
}

bool test08_reversa(){

		unsigned int testNumber = 8;
    string testName = "reversa";    
    printBeginTest(testNumber, testName);

		int a[5] = {123, 3, 0, -1, -123};
		int b[5] = {-123, -1, 0, 3, 123};
		Lista l1(a,5);
		Lista l2(b,5);
		l1.reversa();
 		
		int c[5] = {-123};
		Lista l3(c,1);
		Lista l4(c,1);
		l3.reversa();

		bool res = asegurar(l1 ==  l2, true);
 		res &= asegurar(l3 == l4, true);
 		return res;
}

bool test09_divirdir_pares_impares(){
		unsigned int testNumber = 9;
    string testName = "dividir_pares_impares";    
    printBeginTest(testNumber, testName);

		int a[5] = {122, 3, 0, -1, -123};
		int b[5] = {122, 0, 3, -1, -123};
		Lista l1(a,5);
		Lista l2(b,5);

		//cout << "l1: " << l1;

		l1.dividir_pares_impares();

		//cout << "l1.dividir_pares_impares: " << l1;
 		
		int c[5] = {-123};
		Lista l3(c,1);
		Lista l4(c,1);
		l3.dividir_pares_impares();
	
		bool res = asegurar(l1 == l2, true);
 		res &= asegurar(l3 == l4, true);
 		return res;
}

bool test10_copiar(){
		unsigned int testNumber = 10;
    string testName = "copiar";    
    printBeginTest(testNumber, testName);

		int a[5] = {122, 3, 0, -1, -123};
		Lista l1(a,5);
		
		Lista l2 = l1.copiar();
		bool res = asegurar(l1 == l2, true);

		l2.agregarAtras(4);
 		res &= asegurar(l1 == l2, false);

		l1.agregarAtras(4);
 		res &= asegurar(l1 == l2, true);
		
 		return res;

}
