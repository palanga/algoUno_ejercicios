#include "lista.h"

Lista::Lista()
{
	_primero = nullptr;
	_ultimo = nullptr;
	_largo = 0;
}

Lista::Nodo *Lista::primero() const
{
	return _primero;
}

Lista::Nodo *Lista::ultimo() const
{
	return _ultimo;
}

Lista::Nodo *Lista::iesimo(unsigned int i) const
{
	unsigned int j = 0;
	Nodo* actual = primero();
	while (j < i) {
		j++;
		actual = actual->posterior;
	}
	return actual;
}

unsigned int Lista::largo() const
{
	return _largo;
}

// inserta e atrás:
void Lista::insertar(int e)
{
	Nodo* nuevo = new Nodo { e , ultimo() , nullptr };
	bool listaVacia = primero() == nullptr;
	if ( listaVacia ) {
		_primero = nuevo;
		_ultimo = nuevo;
		_largo = 1;
	}
	if ( !listaVacia ) {
		ultimo()->posterior = nuevo;
		_ultimo = nuevo;
		_largo++;
	}
}

// requiere 0<= i <= n
void Lista::insertar(int e, unsigned int i)
{
	unsigned int n = largo();
	bool quieroEncolar = i == n;

	if ( quieroEncolar ) {// 0 <= i == n
		insertar(e);
	}
	if ( !quieroEncolar ) {// 0 <= i < n
		bool quieroAgregarAdelante = i == 0;

		if ( quieroAgregarAdelante ) {// i == 0 < n
			Nodo* nuevo = new Nodo { e , nullptr , primero() };
			primero()->anterior = nuevo;
			_primero = nuevo;
			_largo++;
		}
		if ( !quieroAgregarAdelante ) {// 0 < i < n
			Nodo* elAnterior = iesimo(i-1);
			Nodo* elPosterior = elAnterior->posterior;
			Nodo* nuevo = new Nodo { e , elAnterior , elPosterior };
			elAnterior->posterior = nuevo;
			elPosterior->anterior = nuevo;
			_largo++;
		}
	}
}

// desencola:
// requiere: 0 < n
void Lista::quitar()
{
	bool tieneUnoSolo = largo() == 1;
	bool tieneMasDeUno = 1 < largo();

	if (tieneUnoSolo) {
		Nodo* elUnico = primero();
		delete elUnico;
		elUnico = nullptr;
		_ultimo = nullptr;
		_primero = nullptr;
		_largo--;
	}

	if ( tieneMasDeUno ) {
		Nodo* elUltimo = ultimo();
		Nodo* elAnterior = elUltimo->anterior;
		elAnterior->posterior = nullptr;
		delete elUltimo;
		elUltimo = nullptr;
		_ultimo = elAnterior;
		_largo--;
	}
}

// requiere: 0 <= i < n
void Lista::quitar(unsigned int i)
{
	unsigned int n = largo();
	bool sacarElUltimo = i == n-1;

	if ( sacarElUltimo ) {// 0 <= i == n-1 < n
		quitar();
	}

	if ( !sacarElUltimo ) {// 0 <= i < n-1 < n
		bool sacarElPrimero = i == 0;

		if ( sacarElPrimero ) {// i == 0 < n-1 < n
			Nodo* elPrimero = primero();
			Nodo* elSegundo = elPrimero->posterior;
			elSegundo->anterior = nullptr;
			_primero = elSegundo;
			_largo--;
			delete elPrimero;
			elPrimero = nullptr;
		}

		if ( !sacarElPrimero ) {// 0 < i < n-1 < n
			Nodo* aBorrar = iesimo(i);
			Nodo* elAnterior = aBorrar->anterior ;
			Nodo* elSiguiente = aBorrar->posterior;
			elAnterior->posterior = elSiguiente;
			elSiguiente->anterior = elAnterior;
			_largo--;
			delete aBorrar;
			aBorrar = nullptr;
		}
	}
}

std::ostream &operator <<(std::ostream &os, const Lista &l)
{
	os << '[';
	// recorro la lista:
	Lista::Nodo* actual = l.primero();
	while (actual != l.ultimo()) {
		os << actual->e << ',';
		actual = actual->posterior;
	}
	// si la lista es vacía ultimo es NULL
	if (l.ultimo() != nullptr) {
		os << l.ultimo()->e;
	}
	os << ']';
	return os;
}
