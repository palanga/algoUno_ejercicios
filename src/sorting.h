#ifndef SORTING_H_INCLUDED
#define SORTING_H_INCLUDED /*value*/

#include <vector>
#include <iostream>


template<class T>
void bubbleSort(std::vector<T> &v);

template<class T>
void insertionSort(std::vector<T> &v);

void countingSort(std::vector<int> &v);


#endif /* SORTING_H_INCLUDED */
