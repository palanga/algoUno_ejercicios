#include "busqueda.h"

#include <vector>
#include <iostream>

template<class T>
int busquedaLineal(std::vector<T>& v, T e)
{
	int res = -1;
	int i = 0;
	int iteraciones = 0;

	while (i < v.size())
	{
		iteraciones++;
		if (v[i] == e)
		{
			res = i;
		}
		i++;
	}

	std::cout << "Cantidad de iteraciones: " << iteraciones << std::endl;
	std::cout << "El elemento está en la posición: " << res << std::endl;

	return res;
}
// instanciar los template:
template int busquedaLineal<int>(std::vector<int> &v, int e);
template int busquedaLineal<float>(std::vector<float> &v, float e);


template<class T>
int busquedaBinaria(std::vector<T>& v, T e)
{
	int maxIndex = v.size() - 1;
	int minIndex = 0;
	int splitPoint = ( maxIndex + minIndex ) / 2;
	int iteraciones = 0;

	while (minIndex + 1 != maxIndex)
	{
		iteraciones++;
		if (e < v[splitPoint]) {
			maxIndex = splitPoint;
		} else {
			minIndex = splitPoint;
		}
		splitPoint = ( maxIndex + minIndex ) / 2;
	}

	std::cout << "Cantidad de iteraciones: " << iteraciones << std::endl;

	int res = - 1;

	if (e == v[minIndex]) {
		res = minIndex;
		std::cout << "El elemento está en la posición: " << res << std::endl;
	} else if (e == v[maxIndex]) {
		res = maxIndex;
		std::cout << "El elemento está en la posición: " << res << std::endl;
	} else {
		std::cout << "El elemento no está" << std::endl;// res = - 1 de antes
	}
	
	return res;
}
/** /
// instanciar los template:
template int busquedaBinaria<int>(std::vector<int> &v, int e);
template int busquedaBinaria<float>(std::vector<float> &v, float e);
/**/

// busquedaBinaria desde hasta
template<class T>
int busquedaBinaria(std::vector<T>& v, T e, int desde, int hasta)
{
	int splitPoint = ( hasta + desde ) / 2;
	int iteraciones = 0;

	while (desde + 1 < hasta)
	{
		iteraciones++;
		if (e < v[splitPoint]) {
			hasta = splitPoint;
		} else {
			desde = splitPoint;
		}
		splitPoint = ( hasta + desde ) / 2;
	}

	//std::cout << "Cantidad de iteraciones: " << iteraciones << std::endl;

	int res = - 1;

	if (e == v[desde]) {
		res = desde;
	//	std::cout << "El elemento está en la posición: " << res << std::endl;
	} else if (e == v[hasta]) {
		res = hasta;
	//	std::cout << "El elemento está en la posición: " << res << std::endl;
	} else {
	//	std::cout << "El elemento no está" << std::endl;// res = - 1 de antes
	}
	
	return res;
}
// instanciar los template:
template int busquedaBinaria<int>(std::vector<int> &v, int e);
template int busquedaBinaria<float>(std::vector<float> &v, float e);

// instanciar los template:
template int busquedaBinaria(std::vector<int> &v, int e, int desde, int hasta);
template int busquedaBinaria(std::vector<float> &v, float e, int desde, int hasta);

/** /
// instanciar los template:
template int busquedaBinariaDesdeHasta(std::vector<int> &v, int e, int desde, int hasta);
template int busquedaBinariaDesdeHasta(std::vector<float> &v, float e, int desde, int hasta);
/**/

// dado un vector de T devuelve el índice del máximo:
// requiere: v.size() >= 1
// requiere: rotadaCrecientemente v
// requiere: sin repetidos v
template<class T>
int iDelMaximoRotada(std::vector<T>& v)
{
	int i, j, s;// i: minimo, j: máximo, s: split point;
	i = 0;
	j = v.size() - 1;
	// el siguiente ciclo es una busqueda binaria:
	while (i + 1 < j) {
		s = (i + j) / 2;
		if ( v[s] > v[j] ) {
			i = s;
		} else {
			j = s;
		}
	}
	int res = i;
	if (v[i] < v[j]) {
		res = j;
	}
	return res;
}
// instancio:
template int iDelMaximoRotada(std::vector<int> &v);