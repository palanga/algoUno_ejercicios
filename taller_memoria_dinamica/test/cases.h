#ifndef CASES_INCLUDED_H
#define CASES_INCLUDED_H

using namespace std;

bool test01_agregarAdelante();

bool test02_agregarAtras();

bool test03_longitud();

bool test04_pertenece();

bool test05_iesimo();

bool test06_maximo();

bool test07_borrar_iesimo();

bool test08_reversa();

bool test09_divirdir_pares_impares();

bool test10_copiar();

#endif //CASES_INCLUDED_H
